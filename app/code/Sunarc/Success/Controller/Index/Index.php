<?php


namespace Sunarc\Success\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
       /* $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__(' heading '));
        $resultPage->getLayout()->getBlock('header');
        $block = $resultPage->getLayout()
            ->createBlock('Sunarc\Quoteble\Block\Success\Index')
            ->setTemplate('Sunarc_Quoteble::success/index.phtml')
            ->toHtml();
        $this->getResponse()->setBody($block);
        return $this->resultPageFactory->create();*/
        $resultPage = $this->resultPageFactory->create();
        $resultPage->addHandle('success_index_index'); //loads the layout of module_custom_customlayout.xml file with its name
        return $resultPage;
        return $this->resultPageFactory->create();
    }
}
