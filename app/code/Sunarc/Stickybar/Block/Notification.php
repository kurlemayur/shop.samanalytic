<?php
namespace Sunarc\Stickybar\Block;

class Notification extends \Magento\Framework\View\Element\Template
{
    private $checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->checkoutSession = $checkoutSession;
    }

    const BASE_CONFIG_PATH = "notification_bar/general/";
    const BASE_FREESHIPPING_STATUS = "carriers/freeshipping/active";
    /*
    * Get Free Shipping price
    * */

    /*
    * Get Setting
    * */
    public function getSetting($path)
    {
        $first_par=self::BASE_CONFIG_PATH.$path;
        $second_par=\Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return $this->_scopeConfig->getValue($first_par, $second_par);
    }

    public function getStoreCurrency()
    {
        return $this->_storeManager->getStore()->getCurrentCurrency()->getCurrencySymbol();
    }

    public function getfreeshippingStatus()
    {
        $first_par=self::BASE_FREESHIPPING_STATUS;
        $second_par=\Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        return $this->_scopeConfig->getValue($first_par, $second_par);
    }
    /*
    * Get difference for free shipping
    * */
    public function getBarMessage()
    {
        return $this->getSetting('content');
    }
}
