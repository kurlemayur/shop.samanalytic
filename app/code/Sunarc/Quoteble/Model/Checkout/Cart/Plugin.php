<?php
namespace Sunarc\Quoteble\Model\Checkout\Cart;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Sunarc\Quoteble\Helper\Data;
use Magento\Checkout\Model\Session;
use Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection;
use Sunarc\Quoteble\Model\Quoteble;
use Magento\Catalog\Model\ProductFactory;

class Plugin
{
    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;

    protected $messageManager;
    protected $sunarcHelper;
    protected $QuotebleCollection;
    protected $QuotebleModel;
    protected $_productloader;


    /**
     * Plugin constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection $QuotebleCollection
     * @param \Sunarc\Quoteble\Model\Quoteble $QuotebleModel
     * @param ManagerInterface $messageManager
     * @param \Magento\Catalog\Model\ProductFactory $_productloader
     * @param Data $sunarcHelper
     */
    public function __construct(
       Session $checkoutSession,
       Collection $QuotebleCollection,
       Quoteble $QuotebleModel,
       ManagerInterface $messageManager,
       ProductFactory $_productloader,
       Data $sunarcHelper
    ) {
        $this->messageManager = $messageManager;
        $this->sunarcHelper = $sunarcHelper;
        $this->QuotebleCollection = $QuotebleCollection;
        $this->QuotebleModel = $QuotebleModel;
        $this->quote = $checkoutSession->getQuote();
        $this->_productloader = $_productloader;


    }


    /**
     * @param $subject
     * @param $productInfo
     * @param null $requestInfo
     * @return array
     * @throws LocalizedException
     */
    public function beforeAddProduct($subject, $productInfo, $requestInfo = null)
    {
        return [$productInfo, $requestInfo];
        $productData = $this->getLoadProduct($productInfo->getId());
        if ($productData->getIsQuoteable()==1) {
            $customerId=$this->sunarcHelper->checkUserLogin();
            if ($customerId  > 0) {
                $data['customer_id'] = $customerId;
            } else {
                $data['guest_id'] = $this->sunarcHelper->getSid();
            }
            if($customerId){
                $this->QuotebleCollection->addFieldToFilter('customer_id',$data['customer_id']);
            }else{
                $this->QuotebleCollection->addFieldToFilter('guest_id',$data['guest_id']);
            }
            $this->QuotebleCollection->addFieldToSelect('*');
            $this->QuotebleCollection->addFieldToFilter('product_id',$productInfo->getId());
            if($this->QuotebleCollection->getSize() > 0){
                $this->updateProduct($requestInfo, $this->QuotebleModel);
            }else{
                $this->newProduct($productInfo, $requestInfo, $data, $this->QuotebleModel);
            }
            $logfile='quote_'.date("d-m-y");
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/'.$logfile.'.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            //echo "here";exit;

            $this->QuotebleModel->save();
            $this->messageManager->addSuccess(__("Product added to quote"));
            $logger->info('Product added to quote');
            throw new LocalizedException(__('Could not add Product to Cart'));
            /*try {

            } catch (\Magento\Framework\Model\Exception $e) {
                $logger->info($e->getMessage());
                $this->messageManager->addErrorMessage($e->getMessage());
                throw new LocalizedException(__($e->getMessage()));
            } catch (\RuntimeException $e) {
                $logger->info($e->getMessage());
                throw new LocalizedException(__($e->getMessage()));
            } catch (\Exception $e) {
                $logger->info($e->getMessage());
                throw new LocalizedException(__($e->getMessage()));
            }*/

        }
        //return true;

    }

    /**
     * @param $id
     * @return $this
     */
    public function getLoadProduct($id)
    {
        return $this->_productloader->create()->load($id);
    }

    /**
     * @param $requestInfo
     * @param $model
     */
    public function updateProduct($requestInfo, $model)
    {
        $data1 = $this->QuotebleCollection->getFirstItem();
        $id = $data1->getId();
        $model->load($id);
        if (array_key_exists("qty", $requestInfo)) {
            $model->setQty($model->getQty() + $requestInfo['qty']);
        } else {
            $model->setQty($model->getQty() + 1);
        }
    }

    /**
     * @param $productInfo
     * @param $requestInfo
     * @param $data
     * @param $model
     */
    public function newProduct($productInfo, $requestInfo, $data, $model)
    {
        $data['product_id'] = $productInfo->getId();
        $data['qty'] = 1;
        if (array_key_exists("qty", $requestInfo)) {
            $data['qty'] = $requestInfo['qty'];
        }
        $data['date'] = time();
        $data['extradata'] = [];

        $model->setData($data);
    }
}