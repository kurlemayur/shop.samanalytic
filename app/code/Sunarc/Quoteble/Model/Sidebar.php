<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Sunarc\Quoteble\Model;

use Magento\Checkout\Helper\Data as HelperData;
use Magento\Checkout\Model\Cart;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Quote\Api\Data\CartItemInterface;
use Magento\Quote\Model\Quote\Address\Total;

/**
 * @deprecated 100.1.0
 */
class Sidebar
{
    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var HelperData
     */
    protected $helperData;

    /**
     * @var ResolverInterface
     */
    protected $resolver;

    /**
     * @var int
     */
    protected $summaryQty;

    protected $QuoteCollection;
    protected $sunarcHelper;



    /**
     * @param Cart $cart
     * @param HelperData $helperData
     * @param ResolverInterface $resolver
     * @codeCoverageIgnore
     */
    public function __construct(
        Cart $cart,
        HelperData $helperData,
        ResolverInterface $resolver,
        \Sunarc\Quoteble\Helper\Data $sunarcHelper,
        \Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection $QuoteCollection
    ) {
        $this->cart = $cart;
        $this->helperData = $helperData;
        $this->resolver = $resolver;
        $this->sunarcHelper=$sunarcHelper;
        $this->QuoteCollection=$QuoteCollection;
    }

    /**
     * Compile response data
     *
     * @param string $error
     * @return array
     */
    public function getResponseData($error = '')
    {
        if (empty($error)) {
            $response = [
                'success' => true,
            ];
        } else {
            $response = [
                'success' => false,
                'error_message' => $error,
            ];
        }
        return $response;
    }

    /**
     * Check if required quote item exist
     *
     * @param int $itemId
     * @throws LocalizedException
     * @return $this
     */
    public function checkQuoteItem($itemId)
    {
        $item = $this->cart->getQuote()->getItemById($itemId);
        if (!$item instanceof CartItemInterface) { //echo "here";exit;
            return $this->getQuotebleItem($itemId);

        }
        return $this;
    }

    /**
     * Remove quote item
     *
     * @param int $itemId
     * @return $this
     */
    public function removeQuoteItem($itemId)
    {
        $this->cart->removeItem($itemId);
        $this->cart->save();
        return $this;
    }

    /**
     * Update quote item
     *
     * @param int $itemId
     * @param int $itemQty
     * @throws LocalizedException
     * @return $this
     */
    public function updateQuoteItem($itemId, $itemQty)
    {
        $itemData = [$itemId => ['qty' => $this->normalize($itemQty)]];
        $this->cart->updateItems($itemData)->save();
        return $this;
    }

    /**
     * Apply normalization filter to item qty value
     *
     * @param int $itemQty
     * @return int|array
     */
    protected function normalize($itemQty)
    {
        if ($itemQty) {
            $filter = new \Zend_Filter_LocalizedToNormalized(
                ['locale' => $this->resolver->getLocale()]
            );
            return $filter->filter($itemQty);
        }
        return $itemQty;
    }

    /**
     * Retrieve summary qty
     *
     * @return int
     */
    protected function getSummaryQty()
    {
        if (!$this->summaryQty) {
            $this->summaryQty = $this->cart->getSummaryQty();
        }
        return $this->summaryQty;
    }

    /**
     * Retrieve summary qty text
     *
     * @return string
     */
    protected function getSummaryText()
    {
        return ($this->getSummaryQty() == 1) ? __(' item') : __(' items');
    }

    /**
     * Retrieve subtotal block html
     *
     * @return string
     */
    protected function getSubtotalHtml()
    {
        $totals = $this->cart->getQuote()->getTotals();
        $subtotal = isset($totals['subtotal']) && $totals['subtotal'] instanceof Total
            ? $totals['subtotal']->getValue()
            : 0;
        return $this->helperData->formatPrice($subtotal);
    }

    public function getQuotebleItem($itemId){
        $cId = $this->getQuotebleCustomerDetails();
        $collection = $this->QuoteCollection;
        $collection->addFieldToSelect('*');
        if ($cId > 0) {/*Registered User collection*/
            $collection->addFieldToFilter('customer_id', $cId);
        } else {/*Guest User collection*/
            $collection->addFieldToFilter('guest_id', $this->getQuotebleCustomerSessionDetails());
        }
        $collection->addFieldToFilter('product_id', $itemId);
        if($collection->getSize() > 0) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($collection->getFirstItem()->getProductId());
            return $product;
        }
        throw new LocalizedException(__('We can\'t find the quote item.'));
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerDetails(){
        return $this->sunarcHelper->checkUserLogin();
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerSessionDetails(){
        return $this->sunarcHelper->getSid();
    }
}
