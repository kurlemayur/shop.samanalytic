<?php
namespace Sunarc\Quoteble\Model;

class AdditionalConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    public function getConfig(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quoteble = $objectManager->get('\Sunarc\Quoteble\Block\Cart\Grid');
        $quotebleData=$quoteble->getCustomQuotebleItem();
        $quotebleHtml='';
        if(is_array($quotebleData)) {
            foreach ($quotebleData[0] as $_item) {
                $imageType = 'product_thumbnail_image';
                $image = $quoteble->getImage($_item, $imageType);
                $quotebleHtml .= '<div class="product customQuoteble">
                <div class="osc-delete" id="delete_quoteble_'.$_item->getId().'" onclick="deleteQuoteCheckout('.$_item->getId().')"></div>
                <div class="before_image_div">
                <span class="product-item-photo" title="' . $_item->getName() . '">
                <span class="product-image-container" >
                    <span class="product-image-wrapper">
                        <img class="product-image-photo" src="' . $image->getImageUrl() . '" alt="' . $_item->getName() . '">
                    </span>
                </span>
                </span>    
            </div>';
            $quotebleHtml .= '<div class="product-item-details">
                                    <strong class="product-item-name">
                                        <span>' . $_item->getName() . '</span>
                                    </strong>
                                    <div class="product-item-pricing">
                                        <div class="details-qty qty">                      
                                            <input min="1" type="number" onchange="updateqtyCheckout('.$_item->getId().',this.value)" size="4" class="item-qty cart-item-qty '.$_item->getId().'-quoteqty-mini" id="'.$_item->getId().'-quoteqty-mini" data-cart-item="'.$_item->getId().'" value="'.$quotebleData[1][$_item->getId()].'" data-cart-item-id="'.$_item->getName().'">      
                                            <div class="control-qty">
                                                <div data-change-type="add" class="osc-add" onclick="addQtyCustom('.$_item->getId().')" ></div>
                                                <div data-change-type="minus" class="osc-minus" onclick="minuesQtyCustom('.$_item->getId().')"></div>
                                            </div>
                                        </div>                                        
                                    </div>
                               </div>';
            }
            $quotebleHtml.='<style>#customQuoteCheckout{border:none!important;clear:both;width:91%;margin:0 auto}table#customQuoteCheckout .product.customQuoteble .product-item-details{width:calc(100% - 100px)!important}table#customQuoteCheckout .product.customQuoteble .product-item-details>*{display:inline-block;vertical-align:middle;float:unset!important}table#customQuoteCheckout .product.customQuoteble .product-item-details .product-item-name span{color:#888!important}table#customQuoteCheckout .product.customQuoteble .product-item-details>.product-item-name{width:39%}table#customQuoteCheckout .product.customQuoteble .product-item-details .details-qty.qty label{display:block}table#customQuoteCheckout .product.customQuoteble .product-item-details .item-qty{border:0}#customQuoteCheckout .before_image_div{float:left;width:20%}#customQuoteCheckout .product-item-photo{float:left}#customQuoteCheckout.product-image-photo{width:70px;height:70px}#customQuoteCheckout .product-image-container{width:70px}#customQuoteCheckout .product-image-wrapper{padding-bottom:100%}#customQuoteCheckout .product-item-details{float:left;width:80%}#customQuoteCheckout .product-item-pricing{float:left}#customQuoteCheckout .control-qty{float:right;width:20px}#customQuoteCheckout .customQuoteble{float: left;width: 100%;}</style>';
        }

        $output['quotebleData'] = $quotebleHtml;
        return $output;

    }
}