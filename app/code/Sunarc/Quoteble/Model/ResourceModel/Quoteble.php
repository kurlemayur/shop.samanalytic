<?php
/**
 * Copyright © 2015 Sunarc. All rights reserved.
 */
namespace Sunarc\Quoteble\Model\ResourceModel;

/**
 * Quoteble resource
 */
class Quoteble extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('quoteble_quoteble', 'id');
    }

  
}
