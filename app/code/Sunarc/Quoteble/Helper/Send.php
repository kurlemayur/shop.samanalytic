<?php

namespace Sunarc\Quoteble\Helper;

use Magecomp\Emailquotepro\Model\EmailproductquoteFactory;
use Magecomp\Emailquotepro\Model\Mail\TransportBuilder;
use Magento\Catalog\Helper\Image;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Sunarc\Quoteble\Helper\Data;
use Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection;
use Magento\Catalog\Model\ResourceModel\Product\Collection as Productcollection;


class Send extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_EMAIL_ADMIN_QUOTE_SENDER = 'emailquote/general/adminemailsender';
    const XML_PATH_EMAIL_ADMIN_QUOTE_NOTIFICATION = 'emailquote/general/adminemailtemplate';
    const XML_PATH_EMAIL_CUSTOMER_FEEDBACK_TEMPLATE = 'emailquote/general/customerFeedbacktemplate';
    const XML_PATH_EMAIL_ADMIN_NAME = 'Admin';
    const XML_PATH_EMAIL_ADMIN_EMAIL = 'emailquote/general/adminmailreceiver';
    const QUOTE_COMMENT = 'Quotable Item Quote';


    protected $scopeConfig;
    protected $_modelStoreManagerInterface;
    protected $_helperImage;
    protected $_helperprice;
    protected $inlineTranslation;
    protected $transportBuilder;
    protected $_modelCart;
    protected $_logLoggerInterface;
    protected $_EmailproductquoteFactory;
    protected $checkoutSession;
    protected $sunarcHelper;
    protected $QuoteCollection;
    protected $productCollection;
    protected $_imageBuilder;


    public function __construct(
        Context $context,
        ScopeConfigInterface $configScopeConfigInterface,
        StoreManagerInterface $modelStoreManagerInterface,
        Image $helperImage,
        PricingHelper $helperprice,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        Cart $modelCart,
        LoggerInterface $logLoggerInterface,
        EmailproductquoteFactory $EmailproductquoteFactory,
        CheckoutSession $checkoutSession,
        Filesystem $filesystem,
        Data $sunarcHelper,
        Collection $QuoteCollection,
        Productcollection $productCollection,
        \Magento\Catalog\Block\Product\ImageBuilder $_imageBuilder,
        StringUtils $string
    )
    {
        $this->scopeConfig = $configScopeConfigInterface;
        $this->_modelStoreManagerInterface = $modelStoreManagerInterface;
        $this->_helperImage = $helperImage;
        $this->_helperprice = $helperprice;
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->_modelCart = $modelCart;
        $this->_logLoggerInterface = $logLoggerInterface;
        $this->_EmailproductquoteFactory = $EmailproductquoteFactory;
        $this->checkoutSession = $checkoutSession;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->string = $string;
        $this->sunarcHelper=$sunarcHelper;
        $this->QuoteCollection=$QuoteCollection;
        $this->productCollection=$productCollection;
        $this->_imageBuilder=$_imageBuilder;
        parent::__construct($context);
        $this->context = $context;

    }

    /**
     * @param $name
     * @param $email
     * @param $contact
     * @param $quoteType
     * @param string $comment
     * @param string $address
     * @return bool|string
     */
    public function sendEmail($name, $email, $contact, $quoteType, $comment='', $address='')
    {
        try {
            /* CREATE QUOTE HTML (START) */
            $quote = $this->checkoutSession->getQuote();
            $imageHelper = $this->_helperImage;
            $qhtml = "<tr style='background-color:#e0e0e0'>";
            $qhtml .= "<th>Photo</th><th>Item</th><th>SKU</th><th>Qty</th><th class='right'>Total</th>";
            $qhtml .= "</tr>";
            $quoteId = $quote->getId();
            $pcollection=[];
            if($quoteId==''){
                return 1;
            }
            /*If request quote only for quotable item */
            if($quoteType==1){
                $cId=$this->getQuotebleCustomerDetails();
                $this->QuoteCollection;
                $this->QuoteCollection->addFieldToSelect('*');
                $this->QuoteCollection->addFieldToFilter('send_quote',array('null' => true));
                if($cId > 0){/*Registered User collection*/
                    $this->QuoteCollection->addFieldToFilter('customer_id',$cId);
                }else{/*Guest User collection*/
                    $this->QuoteCollection->addFieldToFilter('guest_id', $this->getQuotebleCustomerSessionDetails());
                }
                if( $this->QuoteCollection->count() > 0) {
                    $pids = [];
                    $pidsQty = [];
                    foreach ( $this->QuoteCollection as $pId) {
                        $pids[] = $pId->getProductId();
                        $pidsQty[$pId->getProductId()] = $pId->getQty();
                    }
                    /** Apply filters here */
                    $pcollection = $this->productCollection->addAttributeToSelect('*')->addFieldToFilter('entity_id', $pids);

                    foreach ($pcollection as $item) {
                        $imageType = 'product_thumbnail_image';
                        //$image = $this->getImage($item, $imageType);
                        $image = $imageHelper->init($item, $imageType)->getUrl();
                        $qhtml .= "<tr>";
                        $qhtml .= "<td style='text-align:center'><img src=" . $image . " alt=" . $item->getName() . " width='100' height='100' /></td>";
                        $qhtml .= "<td style='text-align:center'>" . $item->getName();
                        $qhtml .= "</td>";
                        $qhtml .= "<td style='text-align:center'>" . $item->getSku() . "</td>";
                        $qhtml .= "<td style='text-align:center'>" . $pidsQty[$item->getId()] . "</td>";
                        $finPrice=$item->getFinalPrice()*$pidsQty[$item->getId()];
                        $qhtml .= "<td style='text-align:center'>" . $this->_helperprice->currency(number_format($finPrice, 2, '.', ''), true, false) . "</td>";
                        $qhtml .= "</tr>";
                    }
                }
            }
            else if($quoteType==2){/*All item send in quotable*/

                $items = $quote->getAllVisibleItems();
                foreach ($items as $item) {
                    $img = $imageHelper->init($item->getProduct(), 'product_page_image_small')->getUrl();
                    $qhtml .= "<tr>";
                    $qhtml .= "<td style='text-align:center'><img src=" . $img . " alt=" . $item->getName() . " width='100' height='100' /></td>";
                    $qhtml .= "<td style='text-align:center'>" . $item->getName();
                    $qhtml .= "</td>";
                    $qhtml .= "<td style='text-align:center'>" . $item->getSku() . "</td>";
                    $qhtml .= "<td style='text-align:center'>" . $item->getQty() . "</td>";
                    $qhtml .= "<td style='text-align:center'>" . $this->_helperprice->currency(number_format($item->getRowTotalInclTax(), 2, '.', ''), true, false) . "</td>";
                    $qhtml .= "</tr>";
                }
                $cId=$this->getQuotebleCustomerDetails();
                $this->QuoteCollection;
                $this->QuoteCollection->addFieldToSelect('*');

                if($cId > 0){/*Registered User collection*/
                    $this->QuoteCollection->addFieldToFilter('customer_id',$cId);
                }else{/*Guest User collection*/
                    $this->QuoteCollection->addFieldToFilter('guest_id', $this->getQuotebleCustomerSessionDetails());
                }
                $this->QuoteCollection->addFieldToFilter('send_quote',array('null' => true));

                if($this->QuoteCollection->count() > 0) {
                    $pids = [];
                    $pidsQty = [];
                    foreach ($this->QuoteCollection as $pId) {
                        $pids[] = $pId->getProductId();
                        $pidsQty[$pId->getProductId()] = $pId->getQty();
                    }

                    /** Apply filters here */
                    $pcollection = $this->productCollection->addAttributeToSelect('*')->addFieldToFilter('entity_id', $pids);

                    if($pcollection->count() > 0) {
                        foreach ($pcollection as $item) {
                            $imageType = 'product_thumbnail_image';
                            $image = $this->getImage($item, $imageType);
                            $qhtml .= "<tr>";
                            $qhtml .= "<td style='text-align:center'><img src=" . $image->getImageUrl() . " alt=" . $item->getName() . " width='100' height='100' /></td>";
                            $qhtml .= "<td style='text-align:center'>" . $item->getName();
                            $qhtml .= "</td>";
                            $qhtml .= "<td style='text-align:center'>" . $item->getSku() . "</td>";
                            $qhtml .= "<td style='text-align:center'>" . $pidsQty[$item->getId()] . "</td>";
                            $finPrice = $item->getFinalPrice() * $pidsQty[$item->getId()];
                            $qhtml .= "<td style='text-align:center'>" . $this->_helperprice->currency(number_format($finPrice, 2, '.', ''), true, false) . "</td>";
                            $qhtml .= "</tr>";
                        }
                    }
                }
            }

            $totals = $quote->getTotals();
            $grandtotal =0;// $totals["grand_total"]->getValue();
            // Send Mail To Admin For This
            $this->inlineTranslation->suspend();
            $storeScope = ScopeInterface::SCOPE_STORE;
            if($comment){
                $quoteComment=$comment;
            }else{
                $quoteComment=self::QUOTE_COMMENT;
            }
            $transport = $this->transportBuilder
                ->setTemplateIdentifier($this->scopeConfig->getValue(self::XML_PATH_EMAIL_ADMIN_QUOTE_NOTIFICATION, $storeScope))
                ->setTemplateOptions(
                    [
                        'area' => 'frontend',
                        'store' => Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'customerName' => $name,
                    'customerEmail' =>$email,
                    'customerPhone' => $contact,
                    'customerComment' =>$quoteComment,
                    'customerAddress' =>$address,
                    'quoteid' => $quoteId,
                    'cartgrid' => $qhtml

                ])
                ->setFrom($this->scopeConfig->getValue(self::XML_PATH_EMAIL_ADMIN_QUOTE_SENDER, $storeScope))
                ->addTo($this->scopeConfig->getValue(self::XML_PATH_EMAIL_ADMIN_EMAIL, $storeScope))
                ->getTransport();
            try {
                $transport->sendMessage();
            } catch (\Exception $e) {
                $this->_logLoggerInterface->debug($e->getMessage());
            }
            $this->inlineTranslation->resume();

            // Send Mail To Customer for Receiving message For This
            $this->inlineTranslation->suspend();
            if($comment){
                $quoteComment=$comment;
            }else{
                $quoteComment=self::QUOTE_COMMENT;
            }
            $transport = $this->transportBuilder
                ->setTemplateIdentifier($this->scopeConfig->getValue(self::XML_PATH_EMAIL_CUSTOMER_FEEDBACK_TEMPLATE, $storeScope))
                ->setTemplateOptions(
                    [
                        'area' => 'frontend',
                        'store' => Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'customerName' => $name,
                    'customerEmail' =>$email,
                    'customerPhone' => $contact,
                    'customerComment' =>$quoteComment,
                    'customerAddress' =>$address,
                    'quoteid' => $quoteId,
                    'cartgrid' => $qhtml
                ])
                ->setFrom($this->scopeConfig->getValue(self::XML_PATH_EMAIL_ADMIN_QUOTE_SENDER, $storeScope))
                ->addTo($email)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
            //Save Data in Our Table : emailproductquote
            $pId = array();
            $pSKU = array();

            if($quoteType==1){
                foreach ($pcollection as $item) {
                    $pId[] = $item->getId();
                    $pSKU[] = $item->getSku();
                }
            }else if($quoteType==2){
                foreach ($quote->getAllItems() as $item) {
                    $pId[] = $item->getProduct()->getId();
                    $pSKU[] = $item->getProduct()->getSku();
                }
                //if($pcollection->getSize() > 0) {
                foreach ($pcollection as $item) {
                    $pId[] = $item->getId();
                    $pSKU[] = $item->getSku();
                }
                //}
            }
            $modelEmailProduct = $this->_EmailproductquoteFactory->create();
            $modelEmailProduct->setQuoteId($quote->getEntityId())
                ->setProductId(implode(",", $pId))
                ->setProductSku(implode(",", $pSKU))
                ->setCustomerEmail($email)
                ->setCustomerName($name)
                ->setTelephone($contact)
                ->setComment($quoteComment)
                ->setGrandTotal($grandtotal)
                ->setStatus(2)
                ->save();


            foreach ($this->QuoteCollection as $qdata) {
                $qdata->setEquoteId($modelEmailProduct->getId());
                $qdata->setExtraData($address);
                $qdata->save();
            }

            if($quoteType==2){
                $itemsQuote = $quote->getAllVisibleItems();
                $cId=$this->getQuotebleCustomerDetails();
                $guest_id =$this->getQuotebleCustomerSessionDetails();
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                foreach ($itemsQuote as $item) {
                    $quotebleModel = $objectManager->create('\Sunarc\Quoteble\Model\Quoteble');
                    $dataNon=[];
                    if($cId > 0){/*Registered User collection*/
                        $dataNon['customer_id'] = $cId;
                    }else{/*Guest User collection*/
                        $dataNon['guest_id'] =$guest_id;
                    }
                    $dataNon['product_id'] = $item->getProduct()->getId();
                    $dataNon['qty'] =$item->getQty();
                    $dataNon['date'] = time();
                    $dataNon['extra_data'] = $address;
                    $dataNon['equote_id'] =$modelEmailProduct->getId();
                    $dataNon['send_quote'] ="Y";
                    $quotebleModel->setData($dataNon);
                    $quotebleModel->save();
                }
                $quote->delete();   /*Remove quote id*/
            }

            return true;
        } catch (\Exception $e) {
            $this->_logLoggerInterface->info($e->getMessage());
            return $e->getMessage();
        }
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerDetails(){
        return $this->sunarcHelper->checkUserLogin();
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerSessionDetails(){
        return $this->sunarcHelper->getSid();
    }

    /**
     * @param $product
     * @param $imageId
     * @param array $attributes
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getImage($product, $imageId, $attributes = [])
    {
        return $this->_imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }
}