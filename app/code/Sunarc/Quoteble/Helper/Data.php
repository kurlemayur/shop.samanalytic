<?php
/**
 * Copyright © 2015 Sunarc . All rights reserved.
 */

namespace Sunarc\Quoteble\Helper;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const GENRAL_ENABLE = 'emailquote/general/enable';
    const CAT_IDS = 'quoteble/general/cate_ids';

    protected $_session;
    /**
     * @var \Magento\Framework\App\Helper\Context
     */
    private $context;

    protected $logsession;
    protected $_storeManager;
    protected $_urlInterface;
    protected $_checkoutSession;
    protected $cartManagementInterface;
    protected $categoryCollectionFactory;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Session\SessionManager $sessionManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Session\SessionManager $sessionManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Checkout\Model\Session $_checkoutSession,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Customer\Model\Session $logsession,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
    )
    {
        $this->_session = $sessionManager;
        $this->logsession = $logsession;
        $this->_storeManager = $storeManager;
        $this->_checkoutSession=$_checkoutSession;
        $this->_urlInterface = $urlInterface;
        $this->cartManagementInterface=$cartManagementInterface;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context);
        $this->context = $context;

    }

    public function getSid()
    {
        if ($this->getQuoteId() == '') {/*Check cart exist or not*/
            return $this->getNewCartId();
        }
        return $this->getQuoteId();
        //return $this->_session->getSessionId();
    }

    public function checkUserLogin(){
        if ($this->logsession->isLoggedIn()) {
            //echo $this->logsession->getCustomer()->getId();exit;
           return $this->logsession->getCustomer()->getId();
        } else {
            return "0";
        }
    }

    /**
     * @return mixed
     */
    public function getWebsiteUrl(){
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_DIRECT_LINK).'quotable';
        /*
        echo $this->_storeManager->getStore()->getBaseUrl() . '<br />';
        echo $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_DIRECT_LINK) . '<br />';
        echo $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . '<br />';
        echo $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_STATIC) . '<br />';*/
    }

    public function getQuoteId(){
        return $this->_checkoutSession->getQuoteId();
    }

    /**
     *
     */
    public function getNewCartId(){
        return $this->_checkoutSession->setQuoteId($this->cartManagementInterface->createEmptyCart());
    }

    public function getcollection(){
        
    }

    //To get all Subcategories Ids of a Parent Category, Used for Downloadable Products
    public function getSubCatIds($Parentcategory)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $catId = $Parentcategory;  //Parent Category ID
        $catIds = array();
        $configCates = $this->scopeConfig->getValue(self::CAT_IDS, ScopeInterface::SCOPE_STORE);
        if(!empty($configCates))
        {
            $_configCates = str_replace(' ', '', $configCates);
            $cateArr = explode(',',$_configCates);
            if(in_array($catId,$cateArr))
            {
                $subCategory = $objectManager->create('Magento\Catalog\Model\Category')->load($catId);
                $subCats = $subCategory->getChildrenCategories();
                if(!empty($subCats))
                {
                    foreach ($subCats as $subcat) {
                        $catIds[] = $subcat->getId();
                    }
                }
            }
            else{
                $checkOtherCates = $this->getSubCatIdOfConfigCat();
                if(in_array($catId,$checkOtherCates))
                {
                    $catIds[] = array_filter($checkOtherCates);
                }
            }
        }

        return $catIds;
    }

    public function getSubCatIdOfConfigCat()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $catIds = array();
        $configCates = $this->scopeConfig->getValue(self::CAT_IDS, ScopeInterface::SCOPE_STORE);
        if(!empty($configCates))
        {
            $_configCates = str_replace(' ', '', $configCates);
            $cateArr = explode(',',$_configCates);
            if(!empty($cateArr))
            {
                foreach ($cateArr as $k => $catId)
                $subCategory = $objectManager->create('Magento\Catalog\Model\Category')->load($catId);
                $subCats = $this->getDescendants($subCategory);
                if(!empty($subCats->getData()))
                {
                    foreach ($subCats->getData() as $k => $subcat) {
                        $catIds[] = $subcat['entity_id'];
                    }
                }
            }
        }

        return $catIds;
    }

    public function getDescendants($category, $levels =20)
    {
        if ((int)$levels < 1) {
            $levels = 1;
        }
        $collection = $this->categoryCollectionFactory->create()
            ->addPathsFilter($category->getPath().'/')
            ->addLevelFilter($category->getLevel() + $levels);
        return $collection;
    }

    public function IsActiveMageCompQuoteExtension()
    {
        return $this->scopeConfig->getValue(self::GENRAL_ENABLE, ScopeInterface::SCOPE_STORE);
    }
}