<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Sunarc\Quoteble\Block\Onepage;

/**
 * One page checkout cart link
 *
 * @api
 * @author      Magento Core Team <core@magentocommerce.com>
 */
use Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection;
use Sunarc\Quoteble\Helper\Data;
use Magento\Customer\Model\Session as CustomerSession;

class Link extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    protected $customersession;


    /**
     * @var \Magento\Checkout\Helper\Data
     */
    protected $_checkoutHelper;

    protected $QuoteCollection;
    protected $sunarcHelper;


    /**
     * @var Quote|null
     */
    protected $_quote = null;
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Checkout\Helper\Data $checkoutHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        Collection $QuoteCollection,
        CustomerSession $customersession,
        Data $sunarcHelper,
        array $data = []
    ) {
        $this->_checkoutHelper = $checkoutHelper;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
        $this->QuoteCollection=$QuoteCollection;
        $this->customersession = $customersession;
        $this->sunarcHelper=$sunarcHelper;
        $this->_isScopePrivate = true;
    }

    /**
     * @return string
     */
    public function getCheckoutUrl()
    {
        return $this->getUrl('checkout');
    }

    /**
     * @return bool
     */
    public function isDisabled()
    {
        return !$this->_checkoutSession->getQuote()->validateMinimumAmount();
    }

    /**
     * @return bool
     */
    public function isPossibleOnepageCheckout()
    {
        return $this->_checkoutHelper->canOnepageCheckout();
    }

    /**
     * Get active quote
     *
     * @return Quote
     */
    public function getQuote()
    {
        if (null === $this->_quote) {
            $this->_quote = $this->_checkoutSession->getQuote();
        }
        return $this->_quote;
    }

    /**
     * Get all cart items
     *
     * @return array
     * @codeCoverageIgnore
     */
    public function getItems()
    {
        return $this->getQuote()->getAllVisibleItems();
    }
    
    
    public function getCustomQuotebleItem(){
        ini_set('display_errors', 1);
        $cId=$this->getQuotebleCustomerDetails();
        $collection = $this->QuoteCollection;
        $collection->addFieldToSelect('*');
        $collection->addFieldToFilter('send_quote',array('null' => true));
        if($cId > 0){/*Registered User collection*/
            $collection->addFieldToFilter('customer_id',$cId);
        }else{/*Guest User collection*/
            $collection->addFieldToFilter('guest_id',$this->getQuotebleCustomerSessionDetails());
        }
        //echo $collection->getSelect();exit;
        if($collection->getSize() > 0){
            return 1;
        }
        return 0;
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerDetails(){
        return $this->sunarcHelper->checkUserLogin();
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerSessionDetails(){
        return $this->sunarcHelper->getSid();
    }

    public function getCustomerIdData()
    {
        if ($this->customersession->isLoggedIn()) {
            $customer = $this->customersession->getCustomer();
            return $customer->getEntityId();
        } else {
            return 0;
        }
    }

    public function getCustomerNameData()
    {
        if ($this->customersession->isLoggedIn()) {
            $customer = $this->customersession->getCustomer();
            return $customer->getFirstname() . ' ' . $customer->getLastname();
        } else {
            return '';
        }
    }

    public function getCustomerEmailData()
    {
        if ($this->customersession->isLoggedIn()) {
            $customer = $this->customersession->getCustomer();

            return $customer->getEmail();
        } else {
            return '';
        }
    }

    public function getCustomerTelephoneData()
    {
        if ($this->customersession->isLoggedIn()) {
            $customer = $this->customersession->getCustomer();
            $address = $customer->getPrimaryBillingAddress();
            if ($address) {
                return $address->getTelephone();
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function getCustomerAddressData()
    {
        if ($this->customersession->isLoggedIn()) {
            $customer = $this->customersession->getCustomer();
            $address = $customer->getPrimaryShippingAddress();
            if ($address) {
                $addressData='';
                $streetAddress=$address->getStreet();
                $addressData.=$streetAddress[0].', ';
                $addressData.=$address->getCity().', ';
                $addressData.=$address->getPostcode().', ';
                $addressData.=$address->getRegion();
                return $addressData;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
    public function getWebsiteUrl(){
        return $this->sunarcHelper->getWebsiteUrl();
    }

}
