<?php
namespace Sunarc\Quoteble\Block\Adminhtml\Quoteble\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
		
        parent::_construct();
        $this->setId('checkmodule_quoteble_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Quoteble Information'));
    }
}