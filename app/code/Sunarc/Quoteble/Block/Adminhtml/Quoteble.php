<?php
namespace Sunarc\Quoteble\Block\Adminhtml;
class Quoteble extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {		
        $this->_controller = 'adminhtml_quoteble';/*block grid.php directory*/
        $this->_blockGroup = 'Sunarc_Quoteble';
        $this->_headerText = __('Quoteble');
        $this->_addButtonLabel = __('Add New Entry'); 
        parent::_construct();		
    }
}
