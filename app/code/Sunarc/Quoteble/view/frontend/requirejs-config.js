var config = {
    map: {
        '*': {
            'Magento_Checkout/template/minicart/content.html': 'Sunarc_Quoteble/template/minicart/content.html',
            'Magento_Checkout/template/minicart/item/default.html': 'Sunarc_Quoteble/template/minicart/item/default.html',
            quoteble: 'Sunarc_Quoteble/js/quoteble.js'
        }
    }
};