<?php
namespace Sunarc\Quoteble\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;

/**
 * Cart source
 */
class Cart extends \Magento\Checkout\CustomerData\Cart
{

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {

        $totals = $this->getQuote()->getTotals();
        $subtotalAmount = $totals['subtotal']->getValue();
        /*Quoteble Item*/

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quoteble = $objectManager->get('\Sunarc\Quoteble\Block\Cart\Grid');
        $quotebleData=$quoteble->getCustomQuotebleItem();
        $quotebleHtml='';
        if(is_array($quotebleData)){
            $size=0;//$quotebleData[0]->getSize();
            foreach ($quotebleData[1] as $data){
                $size+=$data;
            }
            foreach ($quotebleData[0] as $_item){
                $imageType = 'product_thumbnail_image';
                $image = $quoteble->getImage($_item, $imageType);
$quotebleHtml.='<div class="product customQuoteble" style="float: left;width: 100%;">
    <div style="float: left;width: 20%;" >
    <a class="product-item-photo" style="float: left;" href="'.$_item->getProductUrl().'" title="'.$_item->getName().'">
    <span class="product-image-container" style="width: 70px;">
        <span class="product-image-wrapper" style="padding-bottom: 100%;">
            <img class="product-image-photo" src="'.$image->getImageUrl().'" alt="'.$_item->getName().'" style="width: 70px; height: 70px;">
        </span>
    </span>
    </a>    
</div>';
                $quotebleHtml.='<div style="float: left;width: 80%;" class="product-item-details">
    <strong class="product-item-name">
        <a href="'.$_item->getProductUrl().'">'.$_item->getName().'</a>
    </strong>
    <div class="product-item-pricing" style="float: left;">
        <div class="details-qty qty">
            <label class="label">Qty:</label>
            <input type="number" onkeyup="showupdate(this.id)" size="4" class="item-qty cart-item-qty" id="'.$_item->getId().'-quoteqty-mini" data-cart-item="'.$_item->getId().'" value="'.$quotebleData[1][$_item->getId()].'" data-cart-item-id="'.$_item->getName().'">
           <button  style="display: none" onclick="updateqty('.$_item->getId().')" class="update-quote-item" id="update-quote-item-'.$_item->getId().'" data-cart-item="'.$_item->getId().'" title="Update">
                <span data-bind="i18n: \'Update\'">Update</span>
            </button>
                           
        </div>
    </div>';
    $quotebleHtml.='<div class="product actions" style="float: left;">
        <div class="secondary">
            <span onclick="deleteQuote('.$_item->getId().')" class="action delete" data-cart-item="'.$_item->getId().'" title="Remove item12">
                <span>Remove</span>
            </span>
        </div>
    </div>
</div>';

            }
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $url=$storeManager->getStore()->getBaseUrl();
            $quotebleHtml.='<style>.product.customQuoteble>div:not(.product-item-details){margin-right:16px!important}.product.customQuoteble .details-qty.qty *{display:inline-block;width:auto}.product.customQuoteble .details-qty.qty input.item-qty{width:40px;height:32px;text-align:center}.product.customQuoteble .product-item-name a{color:#222!important}.product.customQuoteble .product-item-details{width:calc(100% - 88px)!important}.product.customQuoteble .action.delete::before{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;font-size:28px;line-height:28px;color:#303030;content:\'\e604\';font-family:icons-blank-theme;vertical-align:middle;display:inline-block;font-weight:400;overflow:hidden;speak:none;text-align:center}.product.customQuoteble .action.delete span{display:none}</style>';

            $quotebleHtml.='<script>function updateqty(e){require(["jquery"],function(o){var t=jQuery("#"+e+"-quoteqty-mini").val(),a=e;jQuery(".loading-mask").toggle(),jQuery(".loading-mask").attr("style","display: block !important;"),o.ajax({showLoader:!0,url:"'.$url.'checkout/sidebar/updateItemqty",type:"POST",data:{form_key:jQuery.cookie("form_key"),item_id:a,item_qty:t},dataType:"json"}).done(function(e){})})}function deleteQuote(o){require(["jquery","Magento_Ui/js/modal/confirm"],function(e,t){t({content:"Are you sure you would like to remove this item from the shopping cart?",modalClass:"classModal",actions:{confirm:function(){e.ajax({showLoader:!0,url:"'.$url.'checkout/sidebar/removeItem",type:"POST",data:{form_key:jQuery.cookie("form_key"),item_id:o},dataType:"json"}).done(function(o){e("body").removeClass("_has-modal")})},cancel:function(){},always:function(){}}})})}         
function showupdate(e){require(["jquery"],function(o){o("#"+e).closest("div").find("button").show()})}</script>';
        }else{
            $size=0;
        }
        $cart_product=$this->getSummaryCount();
        if($this->getSummaryCount()==0){
            $summary=$size;
        }else{
            $summary=$this->getSummaryCount() + $size;
        }
        return [
            'summary_count' => $summary,
            'subtotalAmount' => $subtotalAmount,
            'subtotal' => isset($totals['subtotal'])
                ? $this->checkoutHelper->formatPrice($subtotalAmount)
                : 0,
            'possible_onepage_checkout' => $this->isPossibleOnepageCheckout(),
            'items' => $this->getRecentItems(),
            'extra_actions' => $this->layout->createBlock(\Magento\Catalog\Block\ShortcutButtons::class)->toHtml(),
            'isGuestCheckoutAllowed' => $this->isGuestCheckoutAllowed(),
            'website_id' => $this->getQuote()->getStore()->getWebsiteId(),
            'request_a_quote' => $size,
            'cart_product' => $cart_product,
            'quotebleHtml' => $quotebleHtml
        ];       
    }

    /**
     * Get active quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    protected function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->checkoutSession->getQuote();
        }
        return $this->quote;
    }

    /**
     * Get shopping cart items qty based on configuration (summary qty or items qty)
     *
     * @return int|float
     */
    protected function getSummaryCount()
    {
        if (!$this->summeryCount) {
            $this->summeryCount = $this->checkoutCart->getSummaryQty() ?: 0;
        }
        return $this->summeryCount;
    }

    /**
     * Check if one page checkout is available
     *
     * @return bool
     */
    protected function isPossibleOnepageCheckout()
    {
        return $this->checkoutHelper->canOnepageCheckout() && !$this->getQuote()->getHasError();
    }

    /**
     * Get array of last added items
     *
     * @return \Magento\Quote\Model\Quote\Item[]
     */
    protected function getRecentItems()
    {
        $items = [];
        if (!$this->getSummaryCount()) {
            return $items;
        }

        foreach (array_reverse($this->getAllQuoteItems()) as $item) {
            if (!$item->getProduct()->isVisibleInSiteVisibility()) {
                $productId = $item->getProduct()->getId();
                $products = $this->catalogUrl->getRewriteByProductStore([$productId => $item->getStoreId()]);
                if (!isset($products[$productId])) {
                    continue;
                }
                $urlDataObject = new \Magento\Framework\DataObject($products[$productId]);
                $item->getProduct()->setUrlDataObject($urlDataObject);
            }
            $items[] = $this->itemPoolInterface->getItemData($item);
        }
        return $items;
    }

    /**
     * Return customer quote items
     *
     * @return \Magento\Quote\Model\Quote\Item[]
     */
    protected function getAllQuoteItems()
    {
        if ($this->getCustomQuote()) {
            return $this->getCustomQuote()->getAllItems();
        }
        return $this->getQuote()->getAllVisibleItems();
    }

    /**
     * Check if guest checkout is allowed
     *
     * @return bool
     */
    public function isGuestCheckoutAllowed()
    {
        return $this->checkoutHelper->isAllowedGuestCheckout($this->checkoutSession->getQuote());
    }
}