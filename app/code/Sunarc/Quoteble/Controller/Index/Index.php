<?php
/**
 *
 * Copyright © 2015 Sunarccommerce. All rights reserved.
 */

namespace Sunarc\Quoteble\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{


    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $sunarchelper;

    protected $_checkoutSession;
    protected $resultJsonFactory;
    protected $regionFactory;
    protected $customerSession;


    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Sunarc\Quoteble\Helper\Data $sunarchelper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Sunarc\Quoteble\Helper\Data $sunarchelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->sunarchelper = $sunarchelper;
        $this->_checkoutSession = $checkoutSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->regionFactory = $regionFactory;
        $this->customerSession = $customerSession;
        $this->customer = $customer;
    }


    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     * @throws \Zend_Validate_Exception
     */
    public function execute()
    {
        $stateAllowed = ['NC', 'SC', 'VA'];/*Shipping Allowed State Details*/
        $this->resultPage = $this->resultPageFactory->create();
        /*If new address*/
        if (!$this->customerSession->isLoggedIn() && $this->getRequest()->getParam('state_id') && trim($this->getRequest()->getParam('state_id')) != '') {
            $error = '';
            if (!\Zend_Validate::is(trim($this->getRequest()->getParam('emailId')), 'EmailAddress'))            {
                return $this->resultJsonFactory->create()->setData(['status' => 4, 'emailId' => 1]);
            }

            if (!\Zend_Validate::is(trim($this->getRequest()->getParam('firstName')), 'NotEmpty')) {
                $error = 'firstName';
            }
            if (!\Zend_Validate::is(trim($this->getRequest()->getParam('lastName')), 'NotEmpty')) {
                $error = 'lastName';
            }
            if (!\Zend_Validate::is(trim($this->getRequest()->getParam('tel')), 'NotEmpty')) {
                $error = 'tel';
            }
            if (!\Zend_Validate::is(trim($this->getRequest()->getParam('city')), 'NotEmpty')) {
                $error = 'city';
            }
            if (!\Zend_Validate::is(trim($this->getRequest()->getParam('postcode')), 'NotEmpty')) {
                $error = 'postcode';
            }
            if (!\Zend_Validate::is(trim($this->getRequest()->getParam('street')), 'NotEmpty')) {
                $error = 'street';
            }
            if (!\Zend_Validate::is(trim($this->getRequest()->getParam('state_id')), 'NotEmpty')) {
                $error = 'state_id';
            }
            if ($error) {
                return $this->resultJsonFactory->create()->setData(['status' => 3, $error => 1]);
            }
            /*If region code not exist in checkout session*/
            return $this->newAddressCheck($stateAllowed);
        } else if (trim($this->_checkoutSession->getQuote()->getShippingAddress()->getRegionCode())) {
            return $this->existAddressCheck($stateAllowed);
        }
        return $this->resultJsonFactory->create()->setData(['status' => 3]);
    }


    /**
     * @param $regionId
     * @return $this
     */
    function getRegionDataById($regionId)
    {
        $region = $this->regionFactory->create();
        return $region->load($regionId);
    }

    /**
     * @param array $stateAllowed
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function newAddressCheck(array $stateAllowed)
    {
        $this->updateCheckoutData();
        /*If first name and email id and  last name blank*/
        /*echo $this->getRequest()->getParam('firstName');
        echo "<pre>"; print_r($this->_checkoutSession->getQuote()->getShippingAddress()->getData());*/
        if (trim($this->_checkoutSession->getQuote()->getShippingAddress()->getEmail()) == '' || trim($this->_checkoutSession->getQuote()->getShippingAddress()->getTelephone()) == '') {
            $this->_checkoutSession->getQuote()->getShippingAddress()->setRegionCode('');
            return $this->resultJsonFactory->create()->setData(['status' => 3]);
        }
        $region = $this->getRegionDataById($this->getRequest()->getParam('state_id'));
        if (in_array($region->getCode(), $stateAllowed) == false) {
            return $this->resultJsonFactory->create()->setData(['status' => 0]);
        }
        return $this->resultJsonFactory->create()->setData(['status' => 1]);
    }


    /**
     * @param array $stateAllowed
     * @return \Magento\Framework\Controller\Result\Json
     * @throws \Zend_Validate_Exception
     */
    public function existAddressCheck(array $stateAllowed)
    {
        /*Check registered user or not*/
        if($this->customerSession->getCustomerId()){
            $customerObj = $this->customer->load($this->customerSession->getCustomerId());
            $customerAddress = 0;
            /*Check registered user have save address or not*/
            foreach ($customerObj->getAddresses() as $address){
                $customerAddress=1;
            }
            /*If address not exist then check input field value*/
            if($customerAddress==0){
                $error='';
                if (!\Zend_Validate::is(trim($this->getRequest()->getParam('firstName')), 'NotEmpty')) {
                    $error = 'firstName';
                }
                if (!\Zend_Validate::is(trim($this->getRequest()->getParam('lastName')), 'NotEmpty')) {
                    $error = 'lastName';
                }
                if (!\Zend_Validate::is(trim($this->getRequest()->getParam('tel')), 'NotEmpty')) {
                    $error = 'tel';
                }
                if (!\Zend_Validate::is(trim($this->getRequest()->getParam('city')), 'NotEmpty')) {
                    $error = 'city';
                }
                if (!\Zend_Validate::is(trim($this->getRequest()->getParam('postcode')), 'NotEmpty')) {
                    $error = 'postcode';
                }
                if (!\Zend_Validate::is(trim($this->getRequest()->getParam('street')), 'NotEmpty')) {
                    $error = 'street';
                }
                if (!\Zend_Validate::is(trim($this->getRequest()->getParam('state_id')), 'NotEmpty')) {
                    $error = 'state_id';
                }
                if ($error) {
                    return $this->resultJsonFactory->create()->setData(['status' => 3, $error => 1]);
                }
            }
        }

        $error = '';
        /*Check Name, email, tel, street address and region code avilable or not*/
        $streetAddress = $this->_checkoutSession->getQuote()->getShippingAddress()->getStreet();
        if (!\Zend_Validate::is(trim($this->_checkoutSession->getQuote()->getShippingAddress()->getName()), 'NotEmpty')) {
            $error = true;
        }
        if (!\Zend_Validate::is(trim($this->_checkoutSession->getQuote()->getShippingAddress()->getTelephone()), 'NotEmpty')) {
            $error = true;
        }
        if(!$this->customerSession->isLoggedIn()) {
            if (!\Zend_Validate::is(trim($this->_checkoutSession->getQuote()->getShippingAddress()->getEmail()), 'EmailAddress')) {
                $error = true;
            }
        }

        if (!\Zend_Validate::is(trim($streetAddress[0]), 'NotEmpty')) {
            $error = true;
        }

        if (!\Zend_Validate::is(trim($this->_checkoutSession->getQuote()->getShippingAddress()->getRegionCode()), 'NotEmpty')) {
            $error = true;
        }

        if ($error) {
            return $this->resultJsonFactory->create()->setData(['status' => 3]);
        }

        /*If region code exist in checkout session for allowed states*/
        if (in_array($this->_checkoutSession->getQuote()->getShippingAddress()->getRegionCode(), $stateAllowed) == false) {
            return $this->resultJsonFactory->create()->setData(['status' => 0]);
        }
        return $this->resultJsonFactory->create()->setData(['status' => 1]);
    }


    /**
     *
     */
    public function updateCheckoutData()
    {
        /*Name Validation*/
        $this->_checkoutSession->getQuote()->getShippingAddress()->setName($this->getRequest()->getParam('firstName') . ' ' . $this->getRequest()->getParam('lastName'));
        /*Email Check and validation*/
        $this->_checkoutSession->getQuote()->getShippingAddress()->setEmail($this->getRequest()->getParam('emailId'));
        /*Telephone Check and validation*/
        $this->_checkoutSession->getQuote()->getShippingAddress()->setTelephone($this->getRequest()->getParam('tel'));
        /*Street Check and validation*/
        $street[] = $this->getRequest()->getParam('street');
        $street[] = $this->getRequest()->getParam('street1');
        $street[] = $this->getRequest()->getParam('street2');
        $this->_checkoutSession->getQuote()->getShippingAddress()->setStreet($street);
        /*City*/
        $this->_checkoutSession->getQuote()->getShippingAddress()->setCity($this->getRequest()->getParam('city'));
        /*Zip code */
        $this->_checkoutSession->getQuote()->getShippingAddress()->setPostcode($this->getRequest()->getParam('postcode'));
    }
}