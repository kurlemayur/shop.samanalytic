<?php
/**
 *
 * Copyright © 2015 Sunarccommerce. All rights reserved.
 */
namespace Sunarc\Quoteble\Controller\Sendquote;
use Psr\Log\LoggerInterface;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $_objectManager;
    protected $_checkoutSession;
    protected $resultJsonFactory;
    protected $sunarcSendHelper;
    protected $sunarcHelper;
    protected $messageManager;
    protected $QuoteCollection;
    protected $regionFactory;
    protected $cartHelper;
    protected $_logLoggerInterface;


    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Sunarc\Quoteble\Helper\Send $sunarcSendHelper
     * @param \Sunarc\Quoteble\Helper\Data $sunarcHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection $QuoteCollection
     * @param \Magento\Checkout\Helper\Cart $cartHelper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param LoggerInterface $logLoggerInterface
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Sunarc\Quoteble\Helper\Send $sunarcSendHelper,
        \Sunarc\Quoteble\Helper\Data $sunarcHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection $QuoteCollection,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        LoggerInterface $logLoggerInterface

    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_objectManager = $objectManager;
        $this->_checkoutSession = $checkoutSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sunarcSendHelper=$sunarcSendHelper;
        $this->sunarcHelper=$sunarcHelper;
        $this->regionFactory = $regionFactory;
        $this->QuoteCollection=$QuoteCollection;
        $this->messageManager = $messageManager;
        $this->cartHelper = $cartHelper;
        $this->_logLoggerInterface = $logLoggerInterface;

    }

    public function execute()
    {
        $name=$this->_checkoutSession->getQuote()->getShippingAddress()->getName();
        $email=$this->_checkoutSession->getQuote()->getShippingAddress()->getEmail();
        $tel=$this->_checkoutSession->getQuote()->getShippingAddress()->getTelephone();
        $address=$comment='';

        if($this->_checkoutSession->getQuote()->getShippingAddress()->getRegion()){
            $state=$this->_checkoutSession->getQuote()->getShippingAddress()->getRegion();
        }else{
            $region=$this->getRegionDataById($this->getRequest()->getParam('state_id'));
            $state=$region->getName();
        }

        $streetAddress=$this->_checkoutSession->getQuote()->getShippingAddress()->getStreet();
        $singleAddress='';
        if (is_array($streetAddress)){
                $singleAddress = implode(',', $streetAddress);

        }
        $address.=$singleAddress.', ';
        $address.=$this->_checkoutSession->getQuote()->getShippingAddress()->getCity().', ';
        $address.=$this->_checkoutSession->getQuote()->getShippingAddress()->getPostcode().', ';
        $address.=$state;
        //echo "<pre>";print_r($this->_checkoutSession->getQuote()->getShippingAddress()->getData());
        if($this->_checkoutSession->getQuote()->getShippingAddress()->getFirstname()=='' || $this->_checkoutSession->getQuote()->getShippingAddress()->getLastname()==''){
            $name=$this->getRequest()->getParam('name');
        }
        //echo $name;
        //  exit;
        if($this->_checkoutSession->getQuote()->getShippingAddress()->getEmail()==''){
            $email=$this->getRequest()->getParam('emailId');
        }

        if($this->_checkoutSession->getQuote()->getShippingAddress()->getTelephone()==''){
            $tel=$this->getRequest()->getParam('tel');
        }
        try {
            $cId = $this->getQuotebleCustomerDetails();
            $collection = $this->QuoteCollection;
            $collection->addFieldToSelect('*');
            if ($cId > 0) {/*Registered User collection*/
                $collection->addFieldToFilter('customer_id', $cId);
            } else {/*Guest User collection*/
                $collection->addFieldToFilter('guest_id', $this->getQuotebleCustomerSessionDetails());
            }
            if($collection->getSize() > 0 || $this->cartHelper->getItemsCount() > 0) {
                try {
                    $emailReturn=$this->sunarcSendHelper->sendEmail($name,$email,$tel,2,$comment,$address);
                    if($emailReturn==1){
                       $this->removeCartItem();/*Remove Cart Item*/
                       $this->removeQuotebleItem();   /*Remove Quoteble Item*/
                    }else{
                       return  $this->resultJsonFactory->create()->setData(['status' =>$emailReturn]);
                    }
                } catch (\Exception $e) {
                    $this->_logLoggerInterface->debug($e->getMessage());
                }

                $this->messageManager->addSuccessMessage(__("Success"));
                return  $this->resultJsonFactory->create()->setData(['status' => 1,'redirectUrl' =>$this->sunarcHelper->getWebsiteUrl()]);
            }
        } catch (\Exception $e) {
            return  $this->resultJsonFactory->create()->setData(['status' => $e->getMessage()]);
        }
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerDetails(){
        return $this->sunarcHelper->checkUserLogin();
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerSessionDetails(){
        return $this->sunarcHelper->getSid();
    }

    /**
     *
     */
    public function removeCartItem()
    {
        $cartObject = $this->_objectManager->create('Magento\Checkout\Model\Cart')->truncate();
        $cartObject->saveQuote();
    }


    /**
     *
     */
    public function removeQuotebleItem()
    {
        $cId = $this->getQuotebleCustomerDetails();
        $collection = $this->QuoteCollection;
        $collection->addFieldToSelect('*');
        $collection->addFieldToFilter('send_quote',array('null' => true));
        if ($cId > 0) {/*Registered User collection*/
            $collection->addFieldToFilter('customer_id', $cId);
        } else {/*Guest User collection*/
            $collection->addFieldToFilter('guest_id', $this->getQuotebleCustomerSessionDetails());
        }
        if($collection->getSize() > 0) {
            foreach ($collection as $data) {
                $data->setSendQuote('Y');
                $data->save();
            }
        }
    }

    function getRegionDataById($regionId){
        $region = $this->regionFactory->create();
        return $region->load($regionId);
    }
}
