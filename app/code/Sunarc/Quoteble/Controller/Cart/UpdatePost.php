<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Sunarc\Quoteble\Controller\Cart;

class UpdatePost extends \Magento\Checkout\Controller\Cart
{
    /**
     * Empty customer's shopping cart
     *
     * @return void
     */
    protected function _emptyShoppingCart()
    {
        try {
            $this->cart->truncate()->save();
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $this->messageManager->addError($exception->getMessage());
        } catch (\Exception $exception) {
            $this->messageManager->addException($exception, __('We can\'t update the shopping cart.'));
        }
    }

    /**
     * Update customer's shopping cart
     *
     * @return void
     */
    protected function _updateShoppingCart()
    {
        try { 
            $cartData = $this->getRequest()->getParam('cart');
            if (is_array($cartData)) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get(
                        \Magento\Framework\Locale\ResolverInterface::class
                    )->getLocale()]
                );
                foreach ($cartData as $index => $data) {
                    if (isset($data['qty'])) {
                        $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
                    }
                }
                if (!$this->cart->getCustomerSession()->getCustomerId() && $this->cart->getQuote()->getCustomerId()) {
                    $this->cart->getQuote()->setCustomerId(null);
                }

                $cartData = $this->cart->suggestItemsQty($cartData);
                $this->cart->updateItems($cartData)->save();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError(
                $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($e->getMessage())
            );
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t update the shopping cart.'));
            $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
        }
    }


    /**
     *
     */
    protected function _updateQuoteShoppingCart()
    {
        try {
            $quoteData = $this->getRequest()->getParam('quote');
            if (is_array($quoteData)) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get(
                        \Magento\Framework\Locale\ResolverInterface::class
                    )->getLocale()]
                );
                $id=[];
                foreach ($quoteData as $index => $data) {
                    if (isset($data['qty'])) {
                        $quoteData[$index]['qty'] = $filter->filter(trim($data['qty']));
                        $id[]=$index;
                    }
                }
                $sunarcHelper=$this->_objectManager->create('Sunarc\Quoteble\Helper\Data');
                $quoteCollection=$this->_objectManager->create('Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection');
                $quoteCollection->addFieldToSelect('*');
                $quoteCollection->addFieldToFilter('send_quote',array('null' => true));
                $customerId = $sunarcHelper->checkUserLogin();
                if ($customerId > 0) {
                    $quoteCollection->addFieldToFilter('customer_id',$customerId);
                } else {
                    $quoteCollection->addFieldToFilter('guest_id',$sunarcHelper->getSid());
                }

                $quoteCollection->addFieldToSelect(array('id','product_id'));
                if($quoteCollection->getSize() > 0){
                    foreach ($quoteCollection as $item){
                        if($quoteData[$item->getProductId()]['qty'] > 0){
                            $item->setQty($quoteData[$item->getProductId()]['qty']);
                            $item->save();
                        }else{
                            $item->delete();
                        }
                    }
                }
                $this->messageManager->addSuccessMessage('We have updated the quote cart.');
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError(
                $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($e->getMessage())
            );
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t update the shopping cart.'));
            $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
        }
    }

    protected function _emptyQuoteShoppingCart()
    {
        try {
            $sunarcHelper=$this->_objectManager->create('Sunarc\Quoteble\Helper\Data');
            $quoteCollection=$this->_objectManager->create('Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection');
            $quoteCollection->addFieldToSelect('*');
            $quoteCollection->addFieldToFilter('send_quote',array('null' => true));
            $customerId = $sunarcHelper->checkUserLogin();
            if ($customerId > 0) {
                $quoteCollection->addFieldToFilter('customer_id',$customerId);
            } else {
                $quoteCollection->addFieldToFilter('guest_id',$sunarcHelper->getSid());
            }
            $quoteCollection->addFieldToSelect(array('id','product_id'));
            if($quoteCollection->getSize() > 0){
                foreach ($quoteCollection as $item){
                    $item->delete();
                }
            }
            $this->messageManager->addSuccessMessage('We have cleared quote.');
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $this->messageManager->addError($exception->getMessage());
        } catch (\Exception $exception) {
            $this->messageManager->addException($exception, __('We can\'t update the quote cart.'));
        }
    }
    /**
     * Update shopping cart data action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $updateAction = (string)$this->getRequest()->getParam('update_cart_action');

        switch ($updateAction) {
            case 'empty_cart':
                $this->_emptyShoppingCart();
                break;
            case 'update_qty':
                $this->_updateShoppingCart();
                break;
            case 'update_quote_qty':
                $this->_updateQuoteShoppingCart();
                break;
            case 'empty_quote':
                $this->_emptyQuoteShoppingCart();
                break;
            default:
                $this->_updateShoppingCart();
        }

        return $this->_goBack();
    }
}
