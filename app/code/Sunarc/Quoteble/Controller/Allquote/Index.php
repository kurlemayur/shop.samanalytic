<?php
/**
 *
 * Copyright © 2015 Sunarccommerce. All rights reserved.
 */
namespace Sunarc\Quoteble\Controller\Allquote;

class Index extends \Magento\Framework\App\Action\Action
{


    protected $resultPageFactory;
    protected $_objectManager;
    protected $_checkoutSession;
    protected $resultJsonFactory;
    protected $sunarcSendHelper;
    protected $sunarcHelper;
    protected $messageManager;
    protected $QuoteCollection;


    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Sunarc\Quoteble\Helper\Send $sunarcSendHelper
     * @param \Sunarc\Quoteble\Helper\Data $sunarcHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection $QuoteCollection
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Sunarc\Quoteble\Helper\Send $sunarcSendHelper,
        \Sunarc\Quoteble\Helper\Data $sunarcHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection $QuoteCollection,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_objectManager = $objectManager;
        $this->_checkoutSession = $checkoutSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sunarcSendHelper=$sunarcSendHelper;
        $this->sunarcHelper=$sunarcHelper;
        $this->QuoteCollection=$QuoteCollection;
        $this->messageManager = $messageManager;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
        $data = $this->_request->getParams();
        $name=$data['customername'];
        $email=$data['customeremail'];
        $tel=$data['telephone'];
        $comment=$data['comment'];
        $address=$data['address'];
        try {
            $this->sunarcSendHelper->sendEmail($name,$email,$tel,1,$comment,$address);
            $this->removeCartItem();/*Remove Cart Item*/
            $this->removeQuotebleItem();/*Remove Quoteble Item*/
            $this->messageManager->addSuccessMessage(__("Your Request Has Been Submitted Successfully."));
            return  $this->resultJsonFactory->create()->setData(['status' => 1]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__("Something Went Wrong, Please Try Later. ".$e->getMessage()));
            return  $this->resultJsonFactory->create()->setData(['status' => 0]);
        }
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerDetails(){
        return $this->sunarcHelper->checkUserLogin();
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerSessionDetails(){
        return $this->sunarcHelper->getSid();
    }

    /**
     *
     */
    public function removeCartItem()
    {
        $cartObject = $this->_objectManager->create('Magento\Checkout\Model\Cart')->truncate();
        $cartObject->saveQuote();
        $quote = $this->_checkoutSession->getQuote();
        $quote->delete();   /*Remove quote id*/
    }


    /**
     *
     */
    public function removeQuotebleItem()
    {
        $collection = '';
        $collection = $this->QuoteCollection;
        $collection->addFieldToSelect('*');
        $collection->addFieldToFilter('send_quote',array('null' => true));
        $cId = $this->getQuotebleCustomerDetails();
        if ($cId > 0) {/*Registered User collection*/
            $collection->addFieldToFilter('customer_id', $cId);
        } else {/*Guest User collection*/
            $collection->addFieldToFilter('guest_id', $this->getQuotebleCustomerSessionDetails());
        }
        if($collection->getSize() > 0) {
            foreach ($collection as $data) {
                $data->setSendQuote('Y');
                $data->save();
            }
        }
    }
}
