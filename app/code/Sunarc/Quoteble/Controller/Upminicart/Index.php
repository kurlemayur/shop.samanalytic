<?php
/**
 *
 * Copyright © 2015 Sunarccommerce. All rights reserved.
 */
namespace Sunarc\Quoteble\Controller\Upminicart;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $sunarchelper;
    protected $resultJsonFactory;
    protected $sunarcSendHelper;
    protected $sunarcHelper;
    protected $QuoteCollection;



   
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Sunarc\Quoteble\Helper\Data $sunarchelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Sunarc\Quoteble\Helper\Send $sunarcSendHelper,
        \Sunarc\Quoteble\Helper\Data $sunarcHelper,
        \Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection $QuoteCollection
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
        $this->sunarchelper=$sunarchelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sunarcSendHelper=$sunarcSendHelper;
        $this->sunarcHelper=$sunarcHelper;
        $this->QuoteCollection=$QuoteCollection;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
        try {
            $params = $this->getRequest()->getParams();
            $cId = $this->getQuotebleCustomerDetails();
            $collection = $this->QuoteCollection;
            $collection->addFieldToSelect('*');
            $collection->addFieldToFilter('send_quote',array('null' => true));
            if ($cId > 0) {/*Registered User collection*/
                $collection->addFieldToFilter('customer_id', $cId);

            } else {/*Guest User collection*/
                $collection->addFieldToFilter('guest_id', $this->getQuotebleCustomerSessionDetails());
            }
            $collection->addFieldToFilter('product_id',$params['id']);

            if($collection->getSize() > 0) {
                foreach ($collection as $data) {
                    if($params['qty'] > 0){
                        $data->setQty($params['qty']);
                        $data->save();
                    }else{
                        $data->delete();
                    }
                }
            }            
            return  $this->resultJsonFactory->create()->setData(['status' => 1]);
        } catch (\Exception $e) {
            return  $this->resultJsonFactory->create()->setData(['status' => $e->getMessage()]);
        }
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerDetails(){
        return $this->sunarcHelper->checkUserLogin();
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerSessionDetails(){
        return $this->sunarcHelper->getSid();
    }
}