<?php
namespace Sunarc\Quoteble\Observer;

use Magento\Framework\Event\ObserverInterface;
class BeforeOrder implements ObserverInterface
{
    protected $_objectManager;
    protected $_checkoutSession;
    protected $sunarcSendHelper;
    protected $sunarcHelper;
    protected $messageManager;
    protected $QuoteCollection;
    protected $cartHelper;
    protected $regionFactory;



    /**
     * BeforeOrder constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Sunarc\Quoteble\Helper\Send $sunarcHelper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Sunarc\Quoteble\Helper\Send $sunarcSendHelper,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Sunarc\Quoteble\Helper\Data $sunarcHelper,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection $QuoteCollection,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_objectManager = $objectManager;
        $this->_checkoutSession = $checkoutSession;
        $this->messageManager = $messageManager;
        $this->sunarcSendHelper=$sunarcSendHelper;
        $this->sunarcHelper=$sunarcHelper;
        $this->QuoteCollection=$QuoteCollection;
        $this->cartHelper = $cartHelper;
        $this->regionFactory = $regionFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $stateAllowed=['NC','SC','VA'];/*Shipping Allowed State Details*/
        $name=$this->_checkoutSession->getQuote()->getShippingAddress()->getName();
        $email=$this->_checkoutSession->getQuote()->getShippingAddress()->getEmail();
        $tel=$this->_checkoutSession->getQuote()->getShippingAddress()->getTelephone();
        $streetAddress=$this->_checkoutSession->getQuote()->getShippingAddress()->getStreet();
        if($this->_checkoutSession->getQuote()->getShippingAddress()->getRegion()){
            $state=$this->_checkoutSession->getQuote()->getShippingAddress()->getRegion();
        }else{
            $region=$this->getRegionDataById($this->getRequest()->getParam('state_id'));
            $state=$region->getName();
        }
        $address=$comment='';
        $address.=$streetAddress[0].', ';
        $address.=$this->_checkoutSession->getQuote()->getShippingAddress()->getCity().', ';
        $address.=$this->_checkoutSession->getQuote()->getShippingAddress()->getPostcode().', ';
        $address.=$state;
        if (in_array($this->_checkoutSession->getQuote()->getShippingAddress()->getRegionCode(), $stateAllowed)==false)
        {
            if ($this->cartHelper->getItemsCount() > 0) {
                //add your logic
                $this->sunarcSendHelper->sendEmail($name, $email, $tel, 2,$comment,$address);
                $this->messageManager->addErrorMessage('Shipping available only for North Carolina, South Carolina and Virginia.');
                $this->removeCartItem();
                $this->removeQuotebleItem();

            }
            die();
        }else{
            /*check quoteble item exist or not*/
            $cId = $this->getQuotebleCustomerDetails();
            $collection = $this->QuoteCollection;
            $collection->addFieldToSelect('*');
            $collection->addFieldToFilter('send_quote',array('null' => true));
            //$collection->addFieldToSelect('id');
            if ($cId > 0) {/*Registered User collection*/
                $collection->addFieldToFilter('customer_id', $cId);
            } else {/*Guest User collection*/
                $collection->addFieldToFilter('guest_id', $this->getQuotebleCustomerSessionDetails());
            }
            if($collection->getSize() > 0) {/*If exist */
                $this->sunarcSendHelper->sendEmail($name,$email,$tel,1,$comment,$address);
                foreach ($collection as $data) {
                    $data->delete();
                }
            }
        }
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerDetails(){
        return $this->sunarcHelper->checkUserLogin();
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerSessionDetails(){
        return $this->sunarcHelper->getSid();
    }
    public function removeQuotebleItem()
    {
        $cId = $this->getQuotebleCustomerDetails();
        $collection = $this->QuoteCollection;
        $collection->addFieldToSelect('*');
        $collection->addFieldToFilter('send_quote',array('null' => true));
       // $collection->addFieldToSelect('id');
        if ($cId > 0) {/*Registered User collection*/
            $collection->addFieldToFilter('customer_id', $cId);
        } else {/*Guest User collection*/
            $collection->addFieldToFilter('guest_id', $this->getQuotebleCustomerSessionDetails());
        }
        if($collection->getSize() > 0) {
            foreach ($collection as $data) {
               // $data->delete();
                $data->setSendQuote('Y');
                $data->save();
            }
        }
    }

    public function removeCartItem()
    {
        $cartObject = $this->_objectManager->create('Magento\Checkout\Model\Cart')->truncate();
        $cartObject->saveQuote();
    }

    function getRegionDataById($regionId){
        $region = $this->regionFactory->create();
        return $region->load($regionId);
    }
}