<?php

namespace Sunarc\Quoteble\Observer;
use Magento\Framework\Event\ObserverInterface;

class CartUpdate implements ObserverInterface
{

    protected $sunarcHelper;
    protected $QuoteCollection;

    /**
     * CartUpdate constructor.
     * @param \Sunarc\Quoteble\Helper\Data $sunarcHelper
     * @param \Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection $QuoteCollection
     */
    public function __construct(
        \Sunarc\Quoteble\Helper\Data $sunarcHelper,
        \Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection $QuoteCollection
    ) {
        $this->sunarcHelper=$sunarcHelper;
        $this->QuoteCollection=$QuoteCollection;
    }


    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws LocalizedException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $infoDataObject = $observer->getEvent()->getInfo()->toArray();
        //echo "<pre>"; print_r($infoDataObject);exit;
        $this->getQuotebleItem($infoDataObject);
    }

    /**
     * @param $params
     * @throws LocalizedException
     */
    public function getQuotebleItem($params){
        foreach ($params as $key=>$value){
            $cId = $this->getQuotebleCustomerDetails();
            $collection = $this->QuoteCollection;
            $collection->addFieldToSelect('*');
           // $collection->addFieldToSelect('*');
            $collection->addFieldToFilter('send_quote',array('null' => true));
            if ($cId > 0) {/*Registered User collection*/
                $collection->addFieldToFilter('customer_id', $cId);
            } else {/*Guest User collection*/
                $collection->addFieldToFilter('guest_id', $this->getQuotebleCustomerSessionDetails());
            }

            $collection->addFieldToFilter('product_id',$key);

            if($collection->getSize() > 0) {
                foreach ($collection as $data) {
                    if($value['qty'] > 0){
                        try {
                            $data->setQty($value['qty']);
                            $data->save();
                        } catch (\Magento\Framework\Model\Exception $e) {
                            throw new LocalizedException(__($e->getMessage()));

                        } catch (\RuntimeException $e) {
                            throw new LocalizedException(__($e->getMessage()));
                        } catch (\Exception $e) {
                            throw new LocalizedException(__($e->getMessage()));
                        }
                    }else{
                        try {
                            $data->delete();
                            return true;
                        } catch (\Magento\Framework\Model\Exception $e) {
                            throw new LocalizedException(__($e->getMessage()));

                        } catch (\RuntimeException $e) {
                            throw new LocalizedException(__($e->getMessage()));
                        } catch (\Exception $e) {
                            throw new LocalizedException(__($e->getMessage()));
                        }
                    }
                }
            }            
        }       
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerDetails(){
        return $this->sunarcHelper->checkUserLogin();
    }

    /**
     * @param $sunarcHelper
     * @return mixed
     */
    public function getQuotebleCustomerSessionDetails(){
        return $this->sunarcHelper->getSid();
    }
}