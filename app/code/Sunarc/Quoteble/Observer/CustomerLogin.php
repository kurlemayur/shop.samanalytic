<?php

namespace Sunarc\Quoteble\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomerLogin implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $messageManager = $objectManager->create('Magento\Framework\Message\ManagerInterface');

        //$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/3dec2018.log');
        //$logger = new \Zend\Log\Logger();
        //$logger->addWriter($writer);

        /*Guest user data*/
        $collection = $objectManager->create('Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection');
        $helper = $objectManager->get('Sunarc\Quoteble\Helper\Data');
        $data=$collection->load(); //echo $helper->getSid();
        $data->addFieldToFilter('guest_id',$helper->getSid());
        $data->addFieldToFilter('customer_id','');


        /*Registered user data*/
        $collection = $objectManager->create('Sunarc\Quoteble\Model\ResourceModel\Quoteble\Collection');
        $data1=$collection->load();
        $data1->addFieldToFilter('customer_id',$customer->getId());
        $updateQuote=$existQuote=$delQuote=[];
        foreach ($data1->getData() as $quote){
            $existQuote[]=$quote['product_id'];
        }
        /*Update guest quote with customer id*/
        foreach ($data->getData() as $quote){
            if (in_array($quote['product_id'], $existQuote)==false) {
                $model = $objectManager->create('Sunarc\Quoteble\Model\Quoteble');
                $model->load($quote['id']);
                $model->setCustomerId($customer->getId());
                try {
                    $model->save();
                } catch (\Magento\Framework\Model\Exception $e) {
                    $messageManager->addError($e->getMessage());
                    throw new LocalizedException(__($e->getMessage()));

                } catch (\RuntimeException $e) {
                    throw new LocalizedException(__($e->getMessage()));
                } catch (\Exception $e) {
                    throw new LocalizedException(__($e->getMessage()));
                }
            }else{
                $updateQuote[$quote['product_id']]['qty']=$quote['qty'];
                $updateQuote[$quote['product_id']]['id']=$quote['id'];
            }
        }

        /*Update exist quote qty*/
        foreach ($data1->getData() as $quote){
            $model = $objectManager->create('Sunarc\Quoteble\Model\Quoteble');
            $model->load($quote['id']);
            if (array_key_exists($quote['product_id'],$updateQuote)){
                try {
                    $model->setQty($quote['qty'] + $updateQuote[$quote['product_id']]['qty']);
                    $model->save();
                    $delQuote[] = $updateQuote[$quote['product_id']]['id'];
                } catch (\Magento\Framework\Model\Exception $e) {
                    $messageManager->addError($e->getMessage());
                    throw new LocalizedException(__($e->getMessage()));
                } catch (\RuntimeException $e) {
                    $messageManager->addError($e->getMessage());
                    //$logger->info('Update exist quote qty ' . $e->getMessage());
                    throw new LocalizedException(__($e->getMessage()));
                } catch (\Exception $e) {
                    $messageManager->addError($e->getMessage());
                    //$logger->info('Update exist quote qty ' . $e->getMessage());
                    throw new LocalizedException(__($e->getMessage()));
                }
            }
        }

        /*Delete quote which already exist*/
        foreach ($delQuote as $del){
            try {
                $model1 = $objectManager->create('Sunarc\Quoteble\Model\Quoteble');
                $model1->load($del);
                $model1->delete();
               // $logger->info('Delete quote which already exist '.$del);
            } catch (\Magento\Framework\Model\Exception $e) {
               // $logger->info('Delete quote which already exist '.$e->getMessage());
            } catch (\RuntimeException $e) {
                //$logger->info('Delete quote which already exist '.$e->getMessage());
            } catch (\Exception $e) {
                //$logger->info('Delete quote which already exist '.$e->getMessage());
            }
        }
    }
}